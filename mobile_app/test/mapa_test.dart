import 'package:test/test.dart';
import 'package:mobile_app/locations.dart' as locations;

bool isGreaterThan(x, y) {
  return x > y ? true : false;
}

void main() {
  test('Array with coordinates should be greater than one', () {
    var hospitals = locations.getHospitals(-16.3612091, -71.5610389);
    expect(isGreaterThan(hospitals.toString().length, 1), true);
  });
}
