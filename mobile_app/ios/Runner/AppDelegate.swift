import UIKit
import Flutter
import GoogleMaps
import Firebase

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyCuTdy1DkA51a1P69raH_ovYl6Jg8oyTfM")
    GeneratedPluginRegistrant.register(with: self)
    if(FirebaseApp.app() == nil){
        FirebaseApp.configure()
    }
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }

  //override init() { FirebaseApp.configure() }

  
}
