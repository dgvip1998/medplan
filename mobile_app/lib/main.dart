import "package:flutter/material.dart";
import 'package:mobile_app/videoplayer.dart';
import "./main_menu.dart";
import "./citas.dart";
import "./mapa.dart";
import "./login.dart";
import "./signup.dart";
import "./info.dart";
import "./main_chat.dart";

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dashboard',
      theme: ThemeData(primarySwatch: Colors.blue),
      initialRoute: "/",
      routes: {
        "/": (context) => MainMenu(),
        "/chat": (context) => MainChat(),
        "/citas": (context) => Citas(),
        "/mapa": (context) => Mapa(),
        "/videoplayer": (context) => Video(),
        "/login": (context) => Login(),
        "/signup": (context) => SignUp(),
        "/info": (context) => Info(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
