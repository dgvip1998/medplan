// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

/// GCP key to access the Places API
const key = 'AIzaSyDDBRXYAaiEM-cc-jEX_4-dDVh5Wh4zhgs';

class Hospitales {
  final String name;
  final double rating;
  final String address;

  Hospitales.fromJson(Map jsonMap)
      : name = jsonMap['name'],
        rating = jsonMap['rating'].toDouble(),
        address = jsonMap['vicinity'];
}

/// Retrieves a stream of places either from the network or local asset
Future<Stream<Hospitales>> getPlaces(double lat, double lng) {
  return key.length > 0 ? getPlacesFromNetwork(lat, lng) : getPlacesFromAsset();
}

/// Retrieves a stream of places from the Google Places API
Future<Stream<Hospitales>> getPlacesFromNetwork(double lat, double lng) async {
  var url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json' +
      '?location=$lat,$lng' +
      '&radius=500&type=hospital' +
      '&key=$key';

  var client = new http.Client();
  var streamedRes = await client.send(new http.Request('get', Uri.parse(url)));

  return streamedRes.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .expand((jsonBody) => (jsonBody as Map)['results'])
      .map((jsonPlace) => new Hospitales.fromJson(jsonPlace));
}

/// Retrieves a stream of places from a local json asset
Future<Stream<Hospitales>> getPlacesFromAsset() async {
  return new Stream.fromFuture(rootBundle.loadString('assets/places.json'))
      .transform(json.decoder)
      .expand((jsonBody) => (jsonBody as Map)['results'])
      .map((jsonPlace) => new Hospitales.fromJson(jsonPlace));
}
