import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/cupertino.dart';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'locations.dart' as locations;

class Mapa extends StatefulWidget {
  @override
  _MapaState createState() => _MapaState();
}

class _MapaState extends State<Mapa> {
  static const bg = const Color(0xFFE9EBED);
  static const strong1 = const Color(0xFF65D7DD);
  static const strong2 = const Color(0xFF2997AE);
  static const emphasis = const Color(0xFF184A75);

  GoogleMapController mapController;

  static LatLng _center;

  final Map<String, Marker> _markers = {};
  Future<void> _onMapCreated(GoogleMapController controller) async {
    final nearbyHospitals =
        await locations.getHospitals(_center.latitude, _center.longitude);
    setState(() {
      _markers.clear();
      for (final hosp in nearbyHospitals) {
        /*
        print(hosp["name"]);
        print(hosp["geometry"]["location"]["lat"]);
        print(hosp["geometry"]["location"]["lng"]);
        print(hosp["vicinity"]);
        print("-----------------------");
        */
        final marker = Marker(
          markerId: MarkerId(hosp["name"]),
          position: LatLng(hosp["geometry"]["location"]["lat"],
              hosp["geometry"]["location"]["lng"]),
          infoWindow: InfoWindow(
            title: hosp["name"],
            snippet: hosp["vicinity"],
          ),
        );
        _markers[hosp["name"]] = marker;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _getUserLocation();
  }

  void _getUserLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    List<Placemark> placemark = await Geolocator()
        .placemarkFromCoordinates(position.latitude, position.longitude);
    setState(() {
      _center = LatLng(position.latitude, position.longitude);
      print('${placemark[0].name}');
      print('${position.latitude}');
      print('${position.longitude}');
    });
  }

  

  MapType _currentMapType = MapType.normal;

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  Widget mapButton(Function function, Icon icon, Color color) {
    return RawMaterialButton(
      onPressed: function,
      child: icon,
      shape: new CircleBorder(),
      elevation: 2.0,
      fillColor: color,
      padding: const EdgeInsets.all(7.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bg,
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: strong1,
          title: Text(
            'MedPlan',
            style: TextStyle(
              color: emphasis,
              fontWeight: FontWeight.w700,
              fontSize: 25.0,
            ),
          ),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: SizedBox.expand(
                //height: MediaQuery.of(context).size.height,
                child: _center == null
                    ? Container(
                        child: Center(
                          child: Text(
                            'loading map..',
                            style: TextStyle(
                                fontFamily: 'Avenir-Medium',
                                color: Colors.grey[400]),
                          ),
                        ),
                      )
                    : Container(
                        child: Stack(children: <Widget>[
                          GoogleMap(
                            onMapCreated: _onMapCreated,
                            mapType: _currentMapType,
                            initialCameraPosition:
                                CameraPosition(target: _center, zoom: 16),
                            markers: _markers.values.toSet(),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Container(
                                margin:
                                    EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
                                child: Column(
                                  children: <Widget>[
                                    mapButton(_onMapTypeButtonPressed,
                                        Icon(Icons.public), Colors.green),
                                  ],
                                )),
                          ),
                        ]),
                      ),
              ),
            )
          ],
        ));
  }
}
